﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rdAdmin.aspx.vb" Inherits="RelatedDocs.rdAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="CustomColors%20-%20Trumbull.css" rel="stylesheet" />
    <title></title>
    
    
</head>
<body>
       
    <form id="form1" runat="server">

    <asp:SqlDataSource ID="sqlAxApps" runat="server" ConnectionString="<%$ ConnectionStrings:sqlConn %>"
        SelectCommand="SELECT appid, appname FROM ae_apps UNION SELECT -1 AS appid, '' AS appname ORDER BY appname" />        

    <asp:SqlDataSource ID="sqlRdQueries" runat="server" ConnectionString="<%$ ConnectionStrings:sqlConn %>"
        SelectCommand="SELECT rd_query_uuid, rd_query_name, rd_execution_order, rd_appid FROM rd_queries WHERE rd_appid = @appid OR @appid = -1 UNION 
            SELECT CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_query_uuid, '' AS rd_query_name, -1 AS rd_execution_order, -1 AS rd_appid ORDER BY rd_execution_order, rd_query_name">
        <SelectParameters>
            <asp:ControlParameter ControlID="cdAxApps" Name="appid" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sqlSourceFields" runat="server" ConnectionString="<%$ ConnectionStrings:sqlConn %>"
        SelectCommand="SELECT colnum, coldesc FROM ae_adefs WHERE appid = @appid UNION SELECT -1 AS colnum, '' AS coldesc ORDER BY colnum">
        <SelectParameters>
            <asp:ControlParameter ControlID="cdAxApps" Name="appid" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="sqlRdQueryDetails" runat="server" ConnectionString="<%$ ConnectionStrings:sqlConn %>"
        SelectCommand="SELECT rd_details_uuid, rd_query_uuid, match_field_id, ax_appid, dest_field_id, rd_criteria, rd_value, rd_label, rd_order FROM rd_query_details WHERE rd_query_uuid = @rd_query_uuid 
            UNION SELECT CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_details_uuid, CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_query_uuid, NULL AS match_field_id, NULL AS ax_appid, NULL AS dest_field_id, '=' AS rd_criteria, NULL AS rd_value, CONVERT(BIT, 0) AS rd_label, -1 AS rd_order WHERE @rd_query_uuid = CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') ORDER BY rd_order">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddRdQueries" Name="rd_query_uuid" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
         

    <div style="text-align:center;font-family: Helvetica Neue,Helvetica;"><h3 style="font-family: Helvetica Neue,Helvetica;color:#095841;">Related Documents Administration</h3></div>
              <div style="color:#4a4a4a">
        <asp:Table ID="Table1" runat="server" width="100%">
            <asp:TableRow>
                <asp:TableCell><asp:RadioButton ID="rbNewRd" GroupName="rbRdGrp" runat="server" Text="New" Checked="true" OnCheckedChanged="rbNewRd_CheckedChanged" AutoPostBack="true" Font-Names="Helvetica Neue,Helvetica;" Font-Bold="true" ForeColor="#4a4a4a" /></asp:TableCell>
                <asp:TableCell><asp:TextBox runat="server" ID="txtRdName" Enabled="true" ></asp:TextBox></asp:TableCell>
		        <asp:TableCell>&nbsp;</asp:TableCell>
		        <asp:TableCell><asp:Label ID="Label1" runat="server" Font-Names="Helvetica Neue,Helvetica;" Font-Bold="true" ForeColor="#4a4a4a" ><strong>Execution Order </strong></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox runat="server" ID="txtExecutionOrder" ></asp:TextBox><asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtExecutionOrder" runat="server" ValidationExpression="^(-?\d+)(\.\d+)?$" ErrorMessage="Numbers only!"></asp:RegularExpressionValidator></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><asp:RadioButton ID="rbExistingRd" GroupName="rbRdGrp" runat="server" Text="Existing" OnCheckedChanged="rbExistingRd_CheckedChanged" AutoPostBack="true" Font-Names="Helvetica Neue,Helvetica;" Font-Bold="true" ForeColor="#4a4a4a" /></asp:TableCell>
                <asp:TableCell><asp:DropDownList runat="server" ID="ddRdQueries" DataSourceID="sqlRdQueries" DataTextField="rd_query_name" DataValueField="rd_query_uuid" Enabled="false" OnSelectedIndexChanged="ddRdQueries_SelectedIndexChanged" OnDataBound="ddRdQueries_DataBound" AutoPostBack="true" ></asp:DropDownList></asp:TableCell>
		        <asp:TableCell>&nbsp;</asp:TableCell>
                <asp:TableCell><asp:Label ID="Label2" runat="server" Font-Names="Helvetica Neue,Helvetica;" Font-Bold="true" ForeColor="#4a4a4a" ><strong>Application </strong></asp:Label></asp:TableCell>
                <asp:TableCell><asp:DropDownList runat="server" ID="cdAxApps" DataSourceID="sqlAxApps" DataTextField="appname"  DataValueField="appid" AutoPostBack="true" ></asp:DropDownList></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>&nbsp;</asp:TableCell>
		        <asp:TableCell>&nbsp;</asp:TableCell>
		        <asp:TableCell>&nbsp;</asp:TableCell>
                <asp:TableCell><asp:Label ID="Label3" runat="server" Font-Names="Helvetica Neue,Helvetica;" Font-Bold="true" ForeColor="#4a4a4a" ><strong>Result Color </strong></asp:Label></asp:TableCell>
                <asp:TableCell>
                    <asp:DropDownList runat="server" ID="ddRdResultColor" AutoPostBack="true" OnLoad="ddRdResultColor_Load" >
                        <asp:ListItem Value="black" style="background-color:black; color:white" Text="Black"></asp:ListItem>
                        <asp:ListItem Value="maroon" style="background-color:maroon; color:white" Text="Maroon"></asp:ListItem>
                        <asp:ListItem Value="red" style="background-color:red" Text="Red"></asp:ListItem>
                        <asp:ListItem Value="olive" style="background-color:olive" Text="Olive"></asp:ListItem>
                        <asp:ListItem Value="yellow" style="background-color:yellow" Text="Yellow"></asp:ListItem>
                        <asp:ListItem Value="green" style="background-color:green" Text="Green"></asp:ListItem>
                        <asp:ListItem Value="lime" style="background-color:lime" Text="Lime"></asp:ListItem>
                        <asp:ListItem Value="teal" style="background-color:teal" Text="Teal"></asp:ListItem>
                        <asp:ListItem Value="aqua" style="background-color:aqua" Text="Aqua"></asp:ListItem>
                        <asp:ListItem Value="navy" style="background-color:navy; color:white" Text="Navy"></asp:ListItem>
                        <asp:ListItem Value="blue" style="background-color:blue; color:white" Text="Blue"></asp:ListItem>
                        <asp:ListItem Value="purple" style="background-color:purple" Text="Purple"></asp:ListItem>
                        <asp:ListItem Value="fuchsia" style="background-color:fuchsia" Text="Fuchsia"></asp:ListItem>
                    </asp:DropDownList>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
                  </div>
        <br />

        <asp:GridView runat="server" ID="gvRelatedFields" AutoGenerateColumns="False" AllowPaging="false" AllowSorting="false" ShowHeaderWhenEmpty="true" OnRowDeleting="gvRelatedFields_RowDeleting" width="100%">
            <HeaderStyle ForeColor="#4a4a4a" BackColor="#e0e0e0" Font-Bold="true" Font-Size="13px" Font-Names="Helvetica" />
            <Columns> 
                <asp:BoundField DataField="rd_details_uuid" HeaderText="UUID" ReadOnly="True" Visible="false" /> 
                <asp:TemplateField HeaderText="Match Field"> 
                    <ItemTemplate> 
                        <asp:DropDownList ID="cbSourceFields" runat="server" DataSourceID="sqlSourceFields" DataTextField="coldesc" DataValueField="colnum" ></asp:DropDownList> 
                    </ItemTemplate> 
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="AX Application"> 
                    <ItemTemplate> 
                        <asp:DropDownList ID="cbApplications" runat="server" DataSourceID="sqlAxApps" DataTextField="appname"  DataValueField="appid" OnSelectedIndexChanged="cbApplications_SelectedIndexChanged" SelectedValue='<%# Bind("ax_appid")%>' AutoPostBack="true" ></asp:DropDownList> 
                    </ItemTemplate> 
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="Index"> 
                    <ItemTemplate> 
                        <asp:DropDownList ID="cbDestFields" runat="server" ></asp:DropDownList> 
                    </ItemTemplate> 
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="Criteria" > 
                    <ItemTemplate> 
                        <asp:DropDownList ID="cbCriteria" runat="server" SelectedValue='<%# Bind("rd_criteria")%>'>
                            <asp:ListItem Text="=" Value="="></asp:ListItem>
                            <asp:ListItem Text=">" Value=">"></asp:ListItem>
                            <asp:ListItem Text="<" Value="<"></asp:ListItem>
                        </asp:DropDownList> 
                    </ItemTemplate> 
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="Filter"> 
                    <ItemTemplate> 
                        <asp:TextBox ID="txtRdValue" runat="server" Text='<%# Bind("rd_value")%>'></asp:TextBox>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Display"> 
                    <ItemTemplate> 
                        <center><asp:CheckBox ID="chkLabel" runat="server" Checked='<%# Bind("rd_label")%>'></asp:CheckBox></center>
                    </ItemTemplate> 
                </asp:TemplateField>
                <asp:CommandField ShowDeleteButton="True" /> 
            </Columns> 
            <EmptyDataTemplate> </EmptyDataTemplate>
        </asp:GridView>
        
        <br />

        <asp:LinkButton runat="server" ID="lbtnAddRow" Text="Add row" OnClick="Unnamed_Click" Font-Names="Helvetica Neue,Helvetica;" Font-Bold="true" ForeColor="#4a4a4a"  />

        <br />
        <br />

        <div style="text-align:center">
            <button id="btnSave" runat="server" class="btn eventbutton btn-primary" style="color:white; background-color:green; margin:2px 2px;" onserverclick="btnSave_Click">Save</button>
            <button id="btnCancel" runat="server" class="btn eventbutton btn-primary" style="color:white; background-color:green; margin:2px 2px;" onclick="if(!confirm('Are you sure you want to cancel without saving your changes?')) return;" onserverclick="btnCancel_Click">Cancel</button>
            <button id="btnDelete" runat="server" class="btn eventbutton btn-primary" style="color:white; background-color:green; margin:2px 2px;" onclick="if(!confirm('Are you sure you want to delete?')) return;" onserverclick="btnDelete_Click">Delete</button>
        </div>
    </form>
</body>
</html>
