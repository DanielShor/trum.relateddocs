﻿Imports System.Data
Imports System.Data.SqlClient

Public Class rdAdmin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dtRdInit As DataTable

        If Not IsPostBack Then
            txtExecutionOrder.Text = 1
            dtRdInit = GetDataTable("SELECT CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_details_uuid, CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_query_uuid, -1 AS match_field_id, -1 AS ax_appid, -1 AS dest_field_id, '=' AS rd_criteria, '' AS rd_value, CONVERT(BIT, 0) AS rd_label, -1 AS rd_order")
            ViewState("CurrentTable") = dtRdInit
            gvRelatedFields.DataSource = dtRdInit
            gvRelatedFields.DataBind()
            'ddRdQueries.Attributes("onChange") = "alert(1);"
            'txtRdName.Attributes("onChange") = "alert(2);"
        End If

    End Sub

    Protected Sub cbApplications_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim dtFields As DataTable
        Dim ddApps As DropDownList
        Dim ddFields As DropDownList

        ddApps = DirectCast(sender, DropDownList)

        dtFields = GetDataTable("SELECT colnum, coldesc FROM ae_adefs WHERE appid = " & ddApps.SelectedValue & " UNION SELECT -1 AS colnum, '' AS coldesc ORDER BY colnum")
        ddFields = DirectCast(DirectCast(ddApps.Parent.Parent, GridViewRow).Cells(3).FindControl("cbDestFields"), DropDownList)
        ddFields.DataSource = dtFields
        ddFields.DataTextField = "coldesc"
        ddFields.DataValueField = "colnum"
        ddFields.DataBind()

    End Sub

    Protected Sub rbExistingRd_CheckedChanged(sender As Object, e As EventArgs)

        ddRdQueries.Enabled = rbExistingRd.Checked
        'txtRdName.Enabled = rbNewRd.Checked
        txtRdName.Text = ""

    End Sub

    Protected Sub rbNewRd_CheckedChanged(sender As Object, e As EventArgs)

        Dim dtRdInit As DataTable

        ddRdQueries.Enabled = rbExistingRd.Checked
        'txtRdName.Enabled = rbNewRd.Checked

        txtExecutionOrder.Text = 1
        txtRdName.Text = ""
        cdAxApps.SelectedValue = -1
        cdAxApps.Enabled = True
        ddRdQueries.SelectedIndex = 0
        ddRdResultColor.SelectedIndex = 0

        dtRdInit = GetDataTable("SELECT CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_details_uuid, CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_query_uuid, -1 AS match_field_id, -1 AS ax_appid, -1 AS dest_field_id, '=' AS rd_criteria, '' AS rd_value, CONVERT(BIT, 0) AS rd_label, -1 AS rd_order")
        ViewState("CurrentTable") = dtRdInit
        gvRelatedFields.DataSource = dtRdInit
        gvRelatedFields.DataBind()

    End Sub

    Protected Sub Unnamed_Click(sender As Object, e As EventArgs)

        Dim rowIndex As Integer = 0

        Dim intCnt As Integer
        Dim dtRdDetails As DataTable
        Dim drRdDetail As DataRow

        If Not IsNothing(ViewState("CurrentTable")) Then
            dtRdDetails = DirectCast(ViewState("CurrentTable"), DataTable)
            If dtRdDetails.Rows.Count > 0 Then
                For intCnt = 0 To dtRdDetails.Rows.Count - 1
                    dtRdDetails.Rows(intCnt)("match_field_id") = IIf(DirectCast(gvRelatedFields.Rows(rowIndex).Cells(1).FindControl("cbSourceFields"), DropDownList).SelectedValue = "", -1, DirectCast(gvRelatedFields.Rows(rowIndex).Cells(1).FindControl("cbSourceFields"), DropDownList).SelectedValue)
                    dtRdDetails.Rows(intCnt)("ax_appid") = DirectCast(gvRelatedFields.Rows(rowIndex).Cells(2).FindControl("cbApplications"), DropDownList).SelectedValue
                    dtRdDetails.Rows(intCnt)("dest_field_id") = IIf(DirectCast(gvRelatedFields.Rows(rowIndex).Cells(3).FindControl("cbDestFields"), DropDownList).SelectedValue = "", -1, DirectCast(gvRelatedFields.Rows(rowIndex).Cells(3).FindControl("cbDestFields"), DropDownList).SelectedValue)
                    dtRdDetails.Rows(intCnt)("rd_criteria") = DirectCast(gvRelatedFields.Rows(rowIndex).Cells(4).FindControl("cbCriteria"), DropDownList).SelectedValue
                    dtRdDetails.Rows(intCnt)("rd_value") = DirectCast(gvRelatedFields.Rows(rowIndex).Cells(5).FindControl("txtRdValue"), TextBox).Text
                    dtRdDetails.Rows(intCnt)("rd_label") = CInt(DirectCast(gvRelatedFields.Rows(rowIndex).Cells(6).FindControl("chkLabel"), CheckBox).Checked)
                    rowIndex = rowIndex + 1
                Next

                drRdDetail = dtRdDetails.Rows.Add()

                drRdDetail("rd_details_uuid") = New Guid("00000000-0000-0000-0000-000000000000")
                drRdDetail("rd_query_uuid") = New Guid(IIf(rbNewRd.Checked, "00000000-0000-0000-0000-000000000000", ddRdQueries.SelectedValue).ToString)
                drRdDetail("match_field_id") = -1
                drRdDetail("ax_appid") = -1
                drRdDetail("dest_field_id") = -1
                drRdDetail("rd_value") = ""
                drRdDetail("rd_criteria") = "="
                drRdDetail("rd_label") = 0
                drRdDetail("rd_order") = rowIndex + 1

                ViewState("CurrentTable") = dtRdDetails
                gvRelatedFields.DataSource = dtRdDetails
                gvRelatedFields.DataBind()
            End If
        End If

        SetPreviousData()

    End Sub

    Public Shared Function GetDataTable(ByVal query As String) As DataTable

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("sqlConn").ConnectionString)
        Dim sqlAdapter As New SqlDataAdapter()

        sqlAdapter.SelectCommand = New SqlCommand(query, sqlConn)

        Dim tblData As New DataTable()

        sqlConn.Open()
        sqlAdapter.Fill(tblData)

        sqlConn.Close()

        Return tblData

    End Function

    Protected Sub ddRdQueries_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim dtRdInit As DataTable
        Dim dtRdQuery As DataTable

        dtRdQuery = GetDataTable("SELECT * FROM rd_queries WHERE rd_query_uuid = CONVERT(UNIQUEIDENTIFIER, '" & ddRdQueries.SelectedValue & "')")

        If dtRdQuery.Rows.Count = 0 Then
            txtExecutionOrder.Text = 1
            txtRdName.Text = ""
            cdAxApps.SelectedValue = -1
            ddRdResultColor.SelectedIndex = 0

            rbNewRd.Checked = True
            rbExistingRd.Checked = False
            ddRdQueries.Enabled = rbExistingRd.Checked
            'txtRdName.Enabled = rbNewRd.Checked

            cdAxApps.Enabled = True

            dtRdInit = GetDataTable("SELECT CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_details_uuid, CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_query_uuid, -1 AS match_field_id, -1 AS ax_appid, -1 AS dest_field_id, '=' AS rd_criteria, '' AS rd_value, CONVERT(BIT, 0) AS rd_label, -1 AS rd_order")
        Else
            txtExecutionOrder.Text = dtRdQuery.Rows(0)("rd_execution_order")
            txtRdName.Text = dtRdQuery.Rows(0)("rd_query_name")
            cdAxApps.SelectedValue = dtRdQuery.Rows(0)("rd_appid")
            ddRdResultColor.SelectedValue = dtRdQuery.Rows(0)("rd_color")

            dtRdInit = GetDataTable("SELECT rd_details_uuid, rd_query_uuid, match_field_id, ax_appid, dest_field_id, RTRIM(rd_criteria) AS rd_criteria, rd_value, rd_label, rd_order FROM rd_query_details WHERE rd_query_uuid = CONVERT(UNIQUEIDENTIFIER, '" & ddRdQueries.SelectedValue & "')")

            cdAxApps.Enabled = False

        End If


        ViewState("CurrentTable") = dtRdInit

        gvRelatedFields.DataSource = dtRdInit
        gvRelatedFields.DataBind()

        SetPreviousData()

    End Sub

    Private Sub SetPreviousData()

        Dim rowIndex As Integer = 0
        Dim intCnt As Integer
        Dim dtRdDetails As DataTable
        Dim dtFields As DataTable

        If Not IsNothing(ViewState("CurrentTable")) Then
            dtRdDetails = DirectCast(ViewState("CurrentTable"), DataTable)

            If dtRdDetails.Rows.Count > 0 Then
                For intCnt = 0 To dtRdDetails.Rows.Count - 1
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(1).FindControl("cbSourceFields"), DropDownList).SelectedValue = dtRdDetails.Rows(intCnt)("match_field_id")
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(2).FindControl("cbApplications"), DropDownList).SelectedValue = dtRdDetails.Rows(intCnt)("ax_appid")
                    dtFields = GetDataTable("SELECT colnum, coldesc FROM ae_adefs WHERE appid = " & DirectCast(gvRelatedFields.Rows(rowIndex).Cells(2).FindControl("cbApplications"), DropDownList).SelectedValue & " UNION SELECT -1 AS colnum, '' AS coldesc ORDER BY colnum")
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(3).FindControl("cbDestFields"), DropDownList).DataSource = dtFields
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(3).FindControl("cbDestFields"), DropDownList).DataTextField = "coldesc"
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(3).FindControl("cbDestFields"), DropDownList).DataValueField = "colnum"
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(3).FindControl("cbDestFields"), DropDownList).DataBind()
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(3).FindControl("cbDestFields"), DropDownList).SelectedValue = dtRdDetails.Rows(intCnt)("dest_field_id")
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(4).FindControl("cbCriteria"), DropDownList).SelectedValue = dtRdDetails.Rows(intCnt)("rd_criteria")
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(5).FindControl("txtRdValue"), TextBox).Text = dtRdDetails.Rows(intCnt)("rd_value")
                    DirectCast(gvRelatedFields.Rows(rowIndex).Cells(6).FindControl("chkLabel"), CheckBox).Checked = dtRdDetails.Rows(intCnt)("rd_label")
                    rowIndex = rowIndex + 1
                Next
            End If
        End If

    End Sub

    Private Function DBSync() As String
        Dim rowIndex As Integer = 0
        Dim intCnt As Integer
        Dim dtRdDetails As DataTable
        Dim strSql As String
        Dim strRdUuid As String

        If Not IsNothing(ViewState("CurrentTable")) Then

            SetRowData()

            dtRdDetails = DirectCast(ViewState("CurrentTable"), DataTable)

            If dtRdDetails.Rows.Count > 0 Then
                strSql = "DELETE FROM rd_query_details WHERE rd_query_uuid = @rd_query_uuid"
                Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("sqlConn").ConnectionString)
                sqlConn.Open()
                Dim sqlComm As New SqlCommand(strSql, sqlConn)

                sqlComm.Parameters.Add(New SqlParameter("@rd_query_uuid", SqlDbType.UniqueIdentifier))
                sqlComm.Parameters("@rd_query_uuid").Value = New Guid(ddRdQueries.SelectedValue)
                sqlComm.ExecuteNonQuery()

                If rbNewRd.Checked Then
                    strRdUuid = GetDataTable("SELECT NEWID() AS uuid").Rows(0)("uuid").ToString
                Else
                    strRdUuid = ddRdQueries.SelectedValue
                    strSql = "DELETE FROM rd_queries WHERE rd_query_uuid = @rd_query_uuid"
                    sqlComm = New SqlCommand(strSql, sqlConn)
                    sqlComm.Parameters.Add(New SqlParameter("@rd_query_uuid", SqlDbType.UniqueIdentifier))
                    sqlComm.Parameters("@rd_query_uuid").Value = New Guid(strRdUuid)
                    sqlComm.ExecuteNonQuery()

                End If

                strSql = "INSERT INTO rd_queries VALUES(@rd_query_uuid, @rd_query_name, @rd_execution_order, @rd_appid, @rd_color)"
                sqlComm = New SqlCommand(strSql, sqlConn)

                sqlComm.Parameters.Add(New SqlParameter("@rd_query_uuid", SqlDbType.UniqueIdentifier))
                sqlComm.Parameters("@rd_query_uuid").Value = New Guid(strRdUuid)

                sqlComm.Parameters.Add(New SqlParameter("@rd_query_name", SqlDbType.VarChar))
                sqlComm.Parameters("@rd_query_name").Value = txtRdName.Text

                sqlComm.Parameters.Add(New SqlParameter("@rd_execution_order", SqlDbType.Int))
                sqlComm.Parameters("@rd_execution_order").Value = IIf(IsNumeric(txtExecutionOrder.Text), txtExecutionOrder.Text, 1)

                sqlComm.Parameters.Add(New SqlParameter("@rd_appid", SqlDbType.Int))
                sqlComm.Parameters("@rd_appid").Value = cdAxApps.SelectedValue

                sqlComm.Parameters.Add(New SqlParameter("@rd_color", SqlDbType.VarChar))
                sqlComm.Parameters("@rd_color").Value = ddRdResultColor.SelectedValue

                sqlComm.ExecuteNonQuery()

                For intCnt = 0 To dtRdDetails.Rows.Count - 1
                    strSql = "INSERT INTO rd_query_details VALUES(NEWID(), @rd_query_uuid, @match_field_id, @ax_appid, @dest_field_id, @rd_criteria, @rd_value, @rd_label, @rd_order)"
                    sqlComm = New SqlCommand(strSql, sqlConn)

                    sqlComm.Parameters.Add(New SqlParameter("@rd_query_uuid", SqlDbType.UniqueIdentifier))
                    sqlComm.Parameters("@rd_query_uuid").Value = New Guid(strRdUuid)

                    sqlComm.Parameters.Add(New SqlParameter("@match_field_id", SqlDbType.Int))
                    sqlComm.Parameters("@match_field_id").Value = dtRdDetails.Rows(intCnt)("match_field_id")

                    sqlComm.Parameters.Add(New SqlParameter("@ax_appid", SqlDbType.Int))
                    sqlComm.Parameters("@ax_appid").Value = dtRdDetails.Rows(intCnt)("ax_appid")

                    sqlComm.Parameters.Add(New SqlParameter("@dest_field_id", SqlDbType.Int))
                    sqlComm.Parameters("@dest_field_id").Value = dtRdDetails.Rows(intCnt)("dest_field_id")

                    sqlComm.Parameters.Add(New SqlParameter("@rd_criteria", SqlDbType.VarChar))
                    sqlComm.Parameters("@rd_criteria").Value = dtRdDetails.Rows(intCnt)("rd_criteria")

                    sqlComm.Parameters.Add(New SqlParameter("@rd_value", SqlDbType.VarChar))
                    sqlComm.Parameters("@rd_value").Value = dtRdDetails.Rows(intCnt)("rd_value")

                    sqlComm.Parameters.Add(New SqlParameter("@rd_label", SqlDbType.Bit))
                    sqlComm.Parameters("@rd_label").Value = dtRdDetails.Rows(intCnt)("rd_label")

                    sqlComm.Parameters.Add(New SqlParameter("@rd_order", SqlDbType.Int))
                    sqlComm.Parameters("@rd_order").Value = intCnt

                    sqlComm.ExecuteNonQuery()
                Next
                sqlConn.Close()

            End If
        End If

        Return strRdUuid

    End Function

    Protected Sub gvRelatedFields_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
        Dim rowIndex As Integer = 0
        Dim dtRdDetails As DataTable

        SetRowData()

        If Not IsNothing(ViewState("CurrentTable")) Then
            dtRdDetails = DirectCast(ViewState("CurrentTable"), DataTable)
            rowIndex = e.RowIndex
            If dtRdDetails.Rows.Count > 1 Then
                dtRdDetails.Rows.Remove(dtRdDetails.Rows(rowIndex))

            Else
                dtRdDetails = GetDataTable("SELECT CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_details_uuid, CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_query_uuid, -1 AS match_field_id, -1 AS ax_appid, -1 AS dest_field_id, '=' AS rd_criteria, '' AS rd_value, CONVERT(BIT, 0) AS rd_label, -1 AS rd_order")
            End If

            ViewState("CurrentTable") = dtRdDetails
            gvRelatedFields.DataSource = dtRdDetails
            gvRelatedFields.DataBind()
            SetPreviousData()
        End If

    End Sub

    Protected Sub SetRowData()
        Dim rowIndex As Integer = 0
        Dim intCnt As Integer
        Dim dtRdDetails As DataTable

        If Not IsNothing(ViewState("CurrentTable")) Then
            dtRdDetails = DirectCast(ViewState("CurrentTable"), DataTable)

            If dtRdDetails.Rows.Count > 0 Then
                For intCnt = 0 To dtRdDetails.Rows.Count - 1
                    dtRdDetails.Rows(intCnt)("match_field_id") = IIf(DirectCast(gvRelatedFields.Rows(rowIndex).Cells(1).FindControl("cbSourceFields"), DropDownList).SelectedValue = "", -1, DirectCast(gvRelatedFields.Rows(rowIndex).Cells(1).FindControl("cbSourceFields"), DropDownList).SelectedValue)
                    dtRdDetails.Rows(intCnt)("ax_appid") = DirectCast(gvRelatedFields.Rows(rowIndex).Cells(2).FindControl("cbApplications"), DropDownList).SelectedValue
                    dtRdDetails.Rows(intCnt)("dest_field_id") = IIf(DirectCast(gvRelatedFields.Rows(rowIndex).Cells(3).FindControl("cbDestFields"), DropDownList).SelectedValue = "", -1, DirectCast(gvRelatedFields.Rows(rowIndex).Cells(3).FindControl("cbDestFields"), DropDownList).SelectedValue)
                    dtRdDetails.Rows(intCnt)("rd_criteria") = DirectCast(gvRelatedFields.Rows(rowIndex).Cells(4).FindControl("cbCriteria"), DropDownList).SelectedValue
                    dtRdDetails.Rows(intCnt)("rd_value") = DirectCast(gvRelatedFields.Rows(rowIndex).Cells(5).FindControl("txtRdValue"), TextBox).Text
                    dtRdDetails.Rows(intCnt)("rd_label") = CInt(DirectCast(gvRelatedFields.Rows(rowIndex).Cells(6).FindControl("chkLabel"), CheckBox).Checked)
                    rowIndex = rowIndex + 1
                Next

                ViewState("CurrentTable") = dtRdDetails
            End If
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Dim dtRdInit As DataTable
        Dim strUuid As String

        strUuid = DBSync()
        ddRdQueries.DataBind()
        If strUuid <> "" Then ddRdQueries.SelectedValue = strUuid

        'txtExecutionOrder.Text = 1
        'txtRdName.Text = ""
        'cdAxApps.SelectedValue = -1
        'ddRdQueries.DataBind()
        'rbNewRd.Checked = True
        'rbExistingRd.Checked = False
        'ddRdQueries.Enabled = rbExistingRd.Checked
        'txtRdName.Enabled = rbNewRd.Checked
        'ddRdResultColor.SelectedIndex = 0

        'dtRdInit = GetDataTable("SELECT CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_details_uuid, CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_query_uuid, -1 AS match_field_id, -1 AS ax_appid, -1 AS dest_field_id, '=' AS rd_criteria, '' AS rd_value, CONVERT(BIT, 0) AS rd_label, -1 AS rd_order")
        dtRdInit = ViewState("CurrentTable")
        gvRelatedFields.DataSource = dtRdInit
        gvRelatedFields.DataBind()
        SetPreviousData()


    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs)
        Dim dtRdInit As DataTable

        'If rbNewRd.Checked Then
        rbNewRd.Checked = True
        'txtRdName.Enabled = True
        ddRdQueries.Enabled = False
        rbExistingRd.Checked = False

        txtExecutionOrder.Text = 1
        txtRdName.Text = ""
        cdAxApps.SelectedValue = -1
        cdAxApps.Enabled = True
        ddRdQueries.SelectedIndex = 0
        ddRdResultColor.SelectedIndex = 0

        dtRdInit = GetDataTable("SELECT CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_details_uuid, CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_query_uuid, -1 AS match_field_id, -1 AS ax_appid, -1 AS dest_field_id, '=' AS rd_criteria, '' AS rd_value, CONVERT(BIT, 0) AS rd_label, -1 AS rd_order")
        ViewState("CurrentTable") = dtRdInit
        gvRelatedFields.DataSource = dtRdInit
        gvRelatedFields.DataBind()

    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
        Dim dtRdInit As DataTable
        Dim strSql As String
        Dim strRdUuid As String

        strSql = "DELETE FROM rd_query_details WHERE rd_query_uuid = @rd_query_uuid"
        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("sqlConn").ConnectionString)
        sqlConn.Open()
        Dim sqlComm As New SqlCommand(strSql, sqlConn)

        sqlComm.Parameters.Add(New SqlParameter("@rd_query_uuid", SqlDbType.UniqueIdentifier))
        sqlComm.Parameters("@rd_query_uuid").Value = New Guid(ddRdQueries.SelectedValue)
        sqlComm.ExecuteNonQuery()

        strRdUuid = ddRdQueries.SelectedValue
        strSql = "DELETE FROM rd_queries WHERE rd_query_uuid = @rd_query_uuid"
        sqlComm = New SqlCommand(strSql, sqlConn)
        sqlComm.Parameters.Add(New SqlParameter("@rd_query_uuid", SqlDbType.UniqueIdentifier))
        sqlComm.Parameters("@rd_query_uuid").Value = New Guid(strRdUuid)
        sqlComm.ExecuteNonQuery()

        txtExecutionOrder.Text = 1
        txtRdName.Text = ""
        cdAxApps.SelectedValue = -1
        cdAxApps.Enabled = False
        ddRdQueries.DataBind()
        rbNewRd.Checked = True
        rbExistingRd.Checked = False
        ddRdQueries.Enabled = rbExistingRd.Checked
        'txtRdName.Enabled = rbNewRd.Checked
        ddRdResultColor.SelectedIndex = 0

        dtRdInit = GetDataTable("SELECT CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_details_uuid, CONVERT(UNIQUEIDENTIFIER, '00000000-0000-0000-0000-000000000000') AS rd_query_uuid, -1 AS match_field_id, -1 AS ax_appid, -1 AS dest_field_id, '=' AS rd_criteria, '' AS rd_value, CONVERT(BIT, 0) AS rd_label, -1 AS rd_order")
        ViewState("CurrentTable") = dtRdInit
        gvRelatedFields.DataSource = dtRdInit
        gvRelatedFields.DataBind()

    End Sub

    Protected Sub ddRdResultColor_Load(sender As Object, e As EventArgs)

        'Dim oItem As ListItem

        'For Each oItem In ddRdResultColor.Items
        'oItem.Attributes.Add("style", "background-color:" & oItem.Value)
        'Next

    End Sub

    Protected Sub ddRdQueries_DataBound(sender As Object, e As EventArgs)
        If txtRdName.Text <> "" And rbExistingRd.Checked = True Then
            If Not IsNothing(ddRdQueries.Items.FindByText(txtRdName.Text)) Then
                ddRdQueries.SelectedValue = ddRdQueries.Items.FindByText(txtRdName.Text).Value
            End If
        End If
    End Sub
End Class