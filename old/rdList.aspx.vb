﻿Imports System.Data
Imports System.Data.SqlClient

Public Class rdList
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim intDocId As Integer
        Dim intAppId As Integer
        Dim intCnt As Integer
        Dim intCnt2 As Integer
        Dim strSql As String
        Dim strWhere As String
        Dim dtQueries As DataTable
        Dim dtQuery As DataTable
        Dim dtDoc As DataTable
        Dim rdCol As New Dictionary(Of String, String)
        Dim rdLabelCol As New Dictionary(Of String, String)
        Dim rdWhereCol As New Dictionary(Of String, String)
        Dim dtRdTemp As DataTable
        Dim dtRdDocs As DataTable
        Dim colColors As New Dictionary(Of Integer, String)

        colColors.Add(0, "#669BEE")
        colColors.Add(1, "#F6740F")
        colColors.Add(2, "#BA1313")
        colColors.Add(3, "#C43BA2")
        colColors.Add(4, "#FFCE4D")

        If Not Request.QueryString("DocId") Is Nothing Then
            intDocId = Request.QueryString("DocId").ToString
            intAppId = Request.QueryString("AppId").ToString
        End If

        strSql = "SELECT * FROM rd_queries WHERE rd_appid = " & intAppId & " ORDER BY rd_execution_order"
        dtQueries = GetDataTable(strSql)

        Response.Write("<div style=""background-color:#0C986F;color:#095841;text-align:center;""><h3>Related Documents</h3></div>")

        For intCnt = 0 To dtQueries.Rows.Count - 1 'Get RD Queries for App
            strSql = "SELECT * FROM rd_query_details WHERE rd_query_uuid = '" & dtQueries.Rows(intCnt)("rd_query_uuid").ToString & "' ORDER BY rd_order"
            dtQuery = GetDataTable(strSql)
            strWhere = ""
            For intCnt2 = 0 To dtQuery.Rows.Count - 1
                If dtQuery(intCnt2)("match_field_id") <> -1 Then
                    If strWhere = "" Then
                        strWhere = " AND ("
                    Else
                        strWhere = strWhere & " AND "
                    End If

                    strWhere = strWhere & "field" & dtQuery(intCnt2)("match_field_id") & " LIKE '" & IIf(dtQuery(intCnt2)("rd_value") = "", "%", Replace(Replace(dtQuery(intCnt2)("rd_value"), "'", "''"), "*", "%")) & "'"
                End If
            Next

            If strWhere <> "" Then strWhere = strWhere & ")"

            strSql = "SELECT * FROM ae_dt" & intAppId & " WHERE docid = " & intDocId & strWhere
            dtDoc = GetDataTable(strSql)

            If dtDoc.Rows.Count > 0 Then 'If I should run RD Query
                rdCol.Clear()
                rdLabelCol.Clear()
                rdWhereCol.Clear()
                For intCnt2 = 0 To dtQuery.Rows.Count - 1

                    If Not rdCol.ContainsKey(dtQuery(intCnt2)("ax_appid")) And dtQuery(intCnt2)("ax_appid") <> -1 Then
                        rdCol.Add(dtQuery(intCnt2)("ax_appid"), "SELECT " & intCnt & " AS q, " & dtQuery(intCnt2)("ax_appid") & " AS appid, (SELECT appname FROM ae_apps WHERE appid = " & dtQuery(intCnt2)("ax_appid") & ") AS appname, docid, @@@ AS rd_label FROM ae_dt" & dtQuery(intCnt2)("ax_appid"))
                        rdLabelCol.Add(dtQuery(intCnt2)("ax_appid"), "")
                        rdWhereCol.Add(dtQuery(intCnt2)("ax_appid"), "")
                    End If
                    If dtQuery(intCnt2)("rd_label") Then 'Build Lable Collection
                        If dtQuery(intCnt2)("ax_appid") = -1 Then
                            If Not rdCol.ContainsKey(intAppId) Then
                                rdCol.Add(intAppId, "SELECT " & intCnt & " AS q, " & intAppId & " AS appid, (SELECT appname FROM ae_apps WHERE appid = " & intAppId & ") AS appname, docid, @@@ AS rd_label FROM ae_dt" & intAppId)
                                rdLabelCol.Add(intAppId, "")
                                rdWhereCol.Add(intAppId, "")
                            End If

                            If rdLabelCol.Item(intAppId) <> "" Then rdLabelCol.Item(intAppId) = rdLabelCol.Item(intAppId) & " + ' ' + "
                            rdLabelCol.Item(intAppId) = rdLabelCol.Item(intAppId) + " ISNULL(field" & dtQuery(intCnt2)("match_field_id") & ", '')"
                        Else
                            If rdLabelCol.Item(dtQuery(intCnt2)("ax_appid")) <> "" Then rdLabelCol.Item(dtQuery(intCnt2)("ax_appid")) = rdLabelCol.Item(dtQuery(intCnt2)("ax_appid")) & " + ' ' + "
                            rdLabelCol.Item(dtQuery(intCnt2)("ax_appid")) = rdLabelCol.Item(dtQuery(intCnt2)("ax_appid")) + " ISNULL(field" & dtQuery(intCnt2)("dest_field_id") & ", '')"
                        End If
                    End If
                    If dtQuery(intCnt2)("rd_value") <> "" Or (dtQuery(intCnt2)("dest_field_id") <> -1 And dtQuery(intCnt2)("match_field_id") <> -1) Then 'If there is a filter value or the fields are linked
                        If dtQuery(intCnt2)("rd_value") <> "" Then
                            If dtQuery(intCnt2)("dest_field_id") <> -1 Then
                                If rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) <> "" Then
                                    rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) = rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) & " AND "
                                Else
                                    rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) = " WHERE "
                                End If
                                rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) = rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) & " field" & dtQuery(intCnt2)("dest_field_id") & " " & dtQuery(intCnt2)("rd_criteria") & " '" & Replace(dtQuery(intCnt2)("rd_value"), "'", "''") & "' "
                            Else
                                If Not rdCol.ContainsKey(intAppId) Then
                                    rdCol.Add(intAppId, "SELECT " & intCnt & " AS q, " & intAppId & " AS appid, docid, @@@ AS rd_label FROM ae_dt" & intAppId)
                                    rdLabelCol.Add(intAppId, "")
                                    rdWhereCol.Add(intAppId, "")
                                End If

                                If rdWhereCol.Item(intAppId) <> "" Then
                                    rdWhereCol.Item(intAppId) = rdWhereCol.Item(intAppId) & " AND "
                                Else
                                    rdWhereCol.Item(intAppId) = " WHERE "
                                End If
                                rdWhereCol.Item(intAppId) = rdWhereCol.Item(intAppId) & " field" & dtQuery(intCnt2)("match_field_id") & " " & dtQuery(intCnt2)("rd_criteria") & " '" & Replace(dtQuery(intCnt2)("rd_value"), "'", "''") & "' "
                            End If
                        Else
                            If rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) <> "" Then
                                rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) = rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) & " AND "
                            Else
                                rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) = " WHERE "
                            End If
                            rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) = rdWhereCol.Item(dtQuery(intCnt2)("ax_appid")) & " field" & dtQuery(intCnt2)("dest_field_id") & " " & dtQuery(intCnt2)("rd_criteria") & " '" & Replace(dtDoc(0)("field" & dtQuery(intCnt2)("match_field_id")), "'", "''") & "' "
                        End If
                    End If

                Next
                For Each oItem As KeyValuePair(Of String, String) In rdCol
                    If rdLabelCol(oItem.Key) <> "" Then
                        strSql = Replace(oItem.Value, "@@@", rdLabelCol(oItem.Key)) & rdWhereCol(oItem.Key)
                        'response.write(strSql & "<br/>")
                        dtRdTemp = GetDataTable(strSql)
                        If IsNothing(dtRdDocs) Then
                            dtRdDocs = dtRdTemp
                        Else
                            dtRdDocs.Merge(dtRdTemp)
                        End If
                    End If
                Next
            End If

        Next

        If Not dtRdDocs Is Nothing Then
            For intCnt = 0 To dtRdDocs.Rows.Count - 1
                Response.Write("<a href=""" & ConfigurationManager.AppSettings("wx") & "?AutoLogoutOnClose=false&DataSource=AXDev&AppName=" & dtRdDocs(intCnt)("appname") & "&DocId=" & dtRdDocs(intCnt)("docid") & """ target=""docview"" style=""text-decoration: none; color: " & colColors(dtRdDocs(intCnt)("q") Mod 5) & ";"">" & dtRdDocs(intCnt)("rd_label") & "</a> <br/>")
            Next

            If dtRdDocs.Rows.Count = 0 Then Response.Write("<div style=""color:#BA1313;"">No related documents were found.</div>")

        End If

        If dtQueries.Rows.Count = 0 Or dtRdDocs Is Nothing Then
            Response.Write("<div style=""color:#BA1313;""><strong>No related documents were found.</strong></div>")
            'Response.Write("<a href=""" & ConfigurationManager.AppSettings("wx") & "?AutoLogoutOnClose=false&DataSource=AXDev&AppName=APENTRY&DocId=7586"" target=""docview"" style=""text-decoration: none; color: " & colColors(1 Mod 5) & ";"" >TEST</a> <br/>")
            'Response.End
        End If
    End Sub

    Public Shared Function GetDataTable(ByVal query As String) As DataTable

        Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("sqlConn").ConnectionString)
        Dim sqlAdapter As New SqlDataAdapter()

        sqlAdapter.SelectCommand = New SqlCommand(query, sqlConn)

        Dim tblData As New DataTable()

        sqlConn.Open()
        sqlAdapter.Fill(tblData)

        sqlConn.Close()

        Return tblData

    End Function


End Class